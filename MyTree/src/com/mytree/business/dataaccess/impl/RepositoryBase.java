/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mytree.business.dataaccess.impl;

import java.io.Serializable;

public abstract class RepositoryBase<TDataSource> implements Serializable {

    private final TDataSource dataSource;

    public RepositoryBase(final TDataSource dataSource) {
        this.dataSource = dataSource;
    }
    
    public RepositoryBase(){
        this.dataSource = null;
    }

    public final TDataSource getDataSource() {
        return dataSource;
    }

}
