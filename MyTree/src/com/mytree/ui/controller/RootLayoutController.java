/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mytree.ui.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import javafx.fxml.FXML;
import com.mytree.assembler.Assembler;
import com.mytree.business.logic.BusinessLogicLocator;
import javafx.stage.Stage;

//Clase para controlar los menúes pricipales
public final class RootLayoutController extends BaseController {

  
    
    
    public RootLayoutController() {
        //constructor sin parametros
    }
    

    @Override
    protected void onInitialize() {
        //no implemenado
    }

    @FXML
    private void handleTreeButton() {
        getNavigationManager().showTree();
    }
    
    @FXML
    private void handleTextualTreeButton() {
        getNavigationManager().showTextualTree();
    }

    @FXML
    private void handleUsersButton() {
        getNavigationManager().showUsers();
    }

    @FXML
    private void handleAttachmentButton() {
        getNavigationManager().showAttachments();
    }
    
    @FXML
    private void handleSalirButton() {
        
        try {
                    File f = new File("datos.save");
                    boolean guardar = true;
                    if (!f.exists()) {
                        guardar =  f.createNewFile();
                    }
                    ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(f));
                    BusinessLogicLocator modelo = BusinessLogicLocator.getInstance();
                    out.writeObject(modelo);
                    out.close();
                } catch (IOException ex) {
                } 
        
    }
    
    
}
