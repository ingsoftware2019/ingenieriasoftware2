
package com.mytree.ui.controller;

import com.mytree.business.logic.BusinessLogicLocator;
import com.mytree.business.model.User;
import com.mytree.ui.model.UserModel;
import com.mytree.utils.Constants;
import java.io.File;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import javafx.fxml.FXML;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public final class UserEditDialogController
        extends BaseController {

    //Se declaran atributos que vienen de la clase fxml (asociada a su ID)
    @FXML
    private Label picturePathLabel;
    @FXML
    private TextField usernameField;
    @FXML
    private TextField firstNameField;
    @FXML
    private TextField secondNameField;
    @FXML
    private TextField firstSurnameField;
    @FXML
    private TextField secondSurnameField;
    @FXML
    private TextField countryField;
    @FXML
    private TextField countryField2;
    @FXML
    private DatePicker birthdayField;
    @FXML
    private DatePicker deceaseField;
    @FXML
    private CheckBox checkDecease;
    @FXML
    private Button cancelButton;

    private Stage dialogStage;
    private FileChooser fileChooser;
    private UserModel userModel;

    public UserEditDialogController() {
        //contstuctor sin parametros

    }

    public void setDialogStage(final Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    //Graba usuario con datos del form
    public void setUser(final UserModel userModel) {
        this.userModel = userModel;
        usernameField.setText(userModel.getUsername().getValue());
        firstNameField.setText(userModel.getFirstName().getValue());
        secondNameField.setText(userModel.getSecondName().getValue());
        firstSurnameField.setText(userModel.getFirstSurname().getValue());
        secondSurnameField.setText(userModel.getSecondSurname().getValue());
        picturePathLabel.setText(userModel.getPicturePath().getValue());
        countryField.setText(userModel.getCountry().getValue());
        countryField2.setText(userModel.getCountry2().getValue());
        birthdayField.setValue(LocalDate.from(
                Instant.ofEpochMilli(userModel.getBirthday().getValue().getTime()).atZone(ZoneId.systemDefault())));
        deceaseField.setValue(LocalDate.from(
                Instant.ofEpochMilli(userModel.getDecease().getValue().getTime()).atZone(ZoneId.systemDefault())));
        checkDecease.setSelected(userModel.getDeceased().getValue());

    }

    //Método para decidir si se puede cancelar o no
    public void allowCancel(final boolean allow) {
        cancelButton.setDisable(!allow);
    }

    @Override
    protected void onInitialize() {
        fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilterJPG = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");
        FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
        fileChooser.getExtensionFilters().addAll(extFilterJPG, extFilterPNG);
        deceaseField.setDisable(true);
    }

    //se realiza el grabado
    @FXML
    private void handleSave() {
        User user = new User();
        user.setId(userModel.getId().getValue());
        user.setUsername(usernameField.getText());
        user.setFirstName(firstNameField.getText());
        user.setSecondName(secondNameField.getText());
        user.setFirstSurname(firstSurnameField.getText());
        user.setSecondSurname(secondSurnameField.getText());
        user.setPicturePath(picturePathLabel.getText());
        user.setCountry(countryField.getText());
        user.setCountry2(countryField2.getText());
        user.setBirthday(Date.from(Instant.from(birthdayField.getValue().atStartOfDay(ZoneId.systemDefault()))));
        user.setDecease(Date.from(Instant.from(deceaseField.getValue().atStartOfDay(ZoneId.systemDefault()))));
        user.setDeceased(checkDecease.isSelected());
        // Make validations
        if (validateUser(user)) {
            BusinessLogicLocator.getInstance().getUserBusinessLogic().save(user);
            dialogStage.close();
        }

    }
    
    @FXML
    private void handleupdateUser() {
        usernameField.setEditable(false);
        User user = BusinessLogicLocator.getInstance().getUserBusinessLogic().getUser(userModel.getId().getValue());
        user.setFirstName(firstNameField.getText());
        user.setSecondName(secondNameField.getText());
        user.setFirstSurname(firstSurnameField.getText());
        user.setSecondSurname(secondSurnameField.getText());
        user.setPicturePath(picturePathLabel.getText());
        user.setCountry(countryField.getText());
        user.setCountry2(countryField2.getText());
        user.setBirthday(Date.from(Instant.from(birthdayField.getValue().atStartOfDay(ZoneId.systemDefault()))));
        user.setDecease(Date.from(Instant.from(deceaseField.getValue().atStartOfDay(ZoneId.systemDefault()))));
        user.setDeceased(checkDecease.isSelected());
        dialogStage.close();
    }

    //Método para cancelar
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    //Método para seleccionar foro de perfil
    @FXML
    private void handleSelectPicture() {
        File file = fileChooser.showOpenDialog(dialogStage);
        picturePathLabel.setText(file.getAbsolutePath());
    }

    //Método que guarda si un usuario falleció o no según el checkbox.
    @FXML
    private void handleDeceased() {

        if (checkDecease.isSelected()) {
            deceaseField.setDisable(false);

        } else {
            deceaseField.setDisable(true);

        }
    }

    //Método que controla si un usuario fue seleccionado para editar.
    private boolean validateUser(final User user) {

        
        StringBuilder resultBuilder = new StringBuilder();
        
        if (user.getUsername().trim().isEmpty()) {
            resultBuilder.append(Constants.USERNAME_REQUIRED);
        }
       
        
        String result = resultBuilder.toString();
        boolean isValidUser = result.isEmpty();
        if (BusinessLogicLocator.getInstance().getUserBusinessLogic().getUsers(isValidUser).contains(user)){
            isValidUser = false;
        }
        
        if (!isValidUser) {
            
            getNavigationManager().showAlert(AlertType.ERROR, null, null, Constants.ADD_USER_ERROR);
        }
        
        return isValidUser;
    }

}
