
package com.mytree.ui.controller;

import com.mytree.business.logic.BusinessLogicLocator;
import com.mytree.business.model.User;
import com.mytree.ui.model.UserModel;
import com.mytree.utils.Constants;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public final class UsersController extends BaseController {

    //Se declaran atributos que vienen de la clase fxml (asociada a su ID)
    @FXML
    private TableView<User> userTable;
    @FXML
    private TableColumn<User, String> nameColumn;
    @FXML
    private Label usernameLabel;
    @FXML
    private Label firstNameLabel;
    @FXML
    private Label secondNameLabel;
    @FXML
    private Label firstSurnameLabel;
    @FXML
    private Label secondSurnameLabel;
    @FXML
    private Label countryLabel;
    @FXML
    private Label countryLabel2;
    @FXML
    private Label birthdayLabel;
    @FXML
    private Label deceaseLabel;
    @FXML
    private Label deceaseTextLabel;
    @FXML
    private Label ageLabel;
    @FXML
    private Label ageTextLabel;
    @FXML
    private ImageView pictureImage;

    public UsersController() {
        //contstuctor sin parametros
    }

    @Override
    protected void onInitialize() {
        nameColumn.setCellValueFactory(cellData -> {
            return new SimpleStringProperty(cellData.getValue().toString());
        });
        userTable.getSelectionModel().selectedItemProperty()
                .addListener((observable, oldValue, newValue) -> showUserDetails(newValue));
        showUserDetails(null);
        reload();
    }

    @FXML
    private void handleNewUser() {
        UserModel userModel = new UserModel(new User());
        getNavigationManager().showUserEditDialog(userModel, true);
        reload();
    }

    @FXML
    private void handleEditUser() {
        User user = checkUserSelection();
        if (user != null) {
            getNavigationManager().showUserEdit(new UserModel(user), true);
            reload();
        }
    }

    private void reload() {
        ObservableList<User> users = FXCollections.observableArrayList();
        BusinessLogicLocator.getInstance().getUserBusinessLogic().getUsers(true).forEach((user) -> {
            users.add(user);
        });
        userTable.setItems(users);
        userTable.refresh();
    }

    private void showUserDetails(final User user) {
        String imagePath = Constants.USER_PROFILE;
        if (user != null) {
            String picturePath = user.getPicturePath();
            if (!user.getDeceased()) {
                deceaseLabel.setText("");
                deceaseTextLabel.setText("");
                ageLabel.setText("");
                ageTextLabel.setText("");
            } else {
                deceaseLabel.setText(DateFormat.getDateInstance(DateFormat.LONG,
                        Locale.getDefault()).format(user.getDecease()));
                deceaseTextLabel.setText("Fecha Fallecimiento");
                ageLabel.setText(deceaseAge(user.getBirthday(), user.getDecease()));
                ageTextLabel.setText("Edad al Fallecer");

            }
            if (picturePath != null && !picturePath.isEmpty()) {
                File file = new File(picturePath);
                imagePath = file.exists() ? file.toURI().toString() : Constants.USER_PROFILE;
            }
            usernameLabel.setText(user.getUsername());
            firstNameLabel.setText(user.getFirstName());
            secondNameLabel.setText(user.getSecondName());
            firstSurnameLabel.setText(user.getFirstSurname());
            secondSurnameLabel.setText(user.getSecondSurname());
            countryLabel.setText(user.getCountry());
            countryLabel2.setText(user.getCountry2());
            birthdayLabel.setText(DateFormat.getDateInstance(DateFormat.LONG,
                    Locale.getDefault()).format(user.getBirthday()));

        } else {
            usernameLabel.setText("");
            firstNameLabel.setText("");
            secondNameLabel.setText("");
            firstSurnameLabel.setText("");
            secondSurnameLabel.setText("");
            countryLabel.setText("");
            countryLabel2.setText("");
            birthdayLabel.setText("");
            deceaseLabel.setText("");
            deceaseTextLabel.setText("");
            ageTextLabel.setText("");
            ageLabel.setText("");
        }
        pictureImage.setImage(new Image(imagePath));
    }

    private User checkUserSelection() {
        User user = userTable.getSelectionModel().getSelectedItem();
        if (user == null) {
            getNavigationManager().showAlert(AlertType.INFORMATION, Constants.WITHOUT_SELECTION,
                    Constants.WITHOUT_USER_SELECTED, Constants.SELECT_USER);
        }
        return user;
    }

    
    private String deceaseAge(Date birthday, Date decease) {
        DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        int d1 = Integer.parseInt(formatter.format(birthday));
        int d2 = Integer.parseInt(formatter.format(decease));
        int deceaseAge = (d2 - d1) / 10000;
        return Integer.toString(deceaseAge);
    }
}
